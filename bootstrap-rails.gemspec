# -*- encoding: utf-8 -*-
require File.expand_path('../lib/bootstrap-rails/version', __FILE__)

Gem::Specification.new do |s|
  s.name        = "bootstrap-rails"
  s.version     = Bootstrap::Rails::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Z KIROKU"]
  s.email       = ["zkiroku@gmail.com"]
  s.homepage    = "https://github.com/zkiroku/bootstrap-rails"
  s.summary     = "Use Twitter Bootstrap with Rails 3"
  s.description = "This gem provides Bootstrap driver for your Rails 3 application."

  s.add_dependency "railties", "~> 3.1"

  s.files        = `git ls-files`.split("\n")
  s.require_path = "lib"
end
